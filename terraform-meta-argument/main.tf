

# # Declare the data source
# data "aws_availability_zones" "available" {
#   state = "available"
# }

# data "aws_key_pair" "NovaMackey" {
#   key_name  = "NovaMac.KP"
# }

locals {
  vpc_id =  aws_vpc.main.id
}


resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = var.vpc_name
  }
}


resource "aws_subnet" "private_subnet" {
  vpc_id     = local.vpc_id
  cidr_block = var.private_subnet_cidr
  availability_zone = var.private_az#1a

  tags = {
    Name = var.private_subnet_name
  }
}


resource "aws_subnet" "private_subnet1" {
  vpc_id     = local.vpc_id
  cidr_block = "10.0.3.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "private_subnet1"
  }
}


resource "aws_subnet" "public_subnet" {
  vpc_id     = local.vpc_id
  cidr_block = var.public_subnet_cidr
  availability_zone = var.public_az#1a
  map_public_ip_on_launch = true

  tags = {
    Name = var.public_subnet_name
  }
}

resource "aws_subnet" "public_subnet1" {
  vpc_id     = local.vpc_id
  cidr_block = "10.0.4.0/24"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = true
  tags = {
    Name = "public_subnet1"
  }
}

resource "aws_subnet" "db_subnet" {
  vpc_id     = local.vpc_id
  cidr_block = "10.0.50.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "db_subnet"
  }
}


resource "aws_subnet" "db_subnet1" {
  vpc_id     = local.vpc_id
  cidr_block = "10.0.53.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "db_subnet1"
  }
}

# HW use locals to change the values of subnets.