
variable "vpc_cidr" {
    type = string
    default = "10.0.0.0/16"
    description = "vpc cidr"
}

variable "private_subnet_cidr" {
    type = string
    default = "10.0.1.0/24"
    description = "private subnet cidr"
}

variable "private_subnet1_cidr" {
    type = string
    default = "10.0.3.0/24"
    description = "private subnet1 cidr"
}

variable "private_subnet2_cidr" {
    type = string
    default = "10.0.5.0/24"
    description = "private subnet2 cidr"
}



variable "public_subnet_cidr" {
    type = string
    default = "10.0.2.0/24"
    description = "public subnet cidr"
}

variable "public_subnet1_cidr" {
    type = string
    default = "10.0.4.0/24"
    description = "public subnet1 cidr"
}

variable "public_subnet2_cidr" {
    type = string
    default = "10.0.6.0/24"
    description = "public subnet2 cidr"
}

variable "vpc_name" {
    type = string
    default = "main_vpc"
    description = "vpc name"
}

variable "private_subnet_name" {
    type = string
    default = "private_subnet"
    description = "private subnet  name"
}

variable "public_subnet_name" {
    type = string
    default = "public_subnet"
    description = "public subnet  name"
}

variable "private_az" {
    type = string
    default = "us-east-1b"
    description = "private subnet az"
  
}

variable "public_az" {
    type = string
    default = "us-east-1c"
    description = "public subnet az"
  
}