
# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_key_pair" "NovaMackey" {
  key_name  = "NovaMac.KP"
}

locals {
  vpc_id =  aws_vpc.main.id
}


resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = var.vpc_name
  }
}


resource "aws_subnet" "private_subnet" {
  vpc_id     = local.vpc_id
  cidr_block = var.private_subnet_cidr
  availability_zone = var.private_az

  tags = {
    Name = var.private_subnet_name
  }
}


resource "aws_subnet" "private_subnet1" {
  vpc_id     = local.vpc_id
  cidr_block = var.private_subnet1_cidr
  availability_zone = var.private_az

  tags = {
    Name = "private_subnet1"
  }
}


resource "aws_subnet" "private_subnet2" {
  vpc_id     = local.vpc_id
  cidr_block = var.private_subnet2_cidr
  availability_zone = "us-east-1f"

  tags = {
    Name = "private_subnet2"
  }
}


resource "aws_subnet" "public_subnet" {
  vpc_id     = local.vpc_id
  cidr_block = var.public_subnet_cidr
  availability_zone = var.public_az
  map_public_ip_on_launch = true

  tags = {
    Name = var.public_subnet_name
  }
}

resource "aws_subnet" "public_subnet1" {
  vpc_id     = local.vpc_id
  cidr_block = var.public_subnet1_cidr
  availability_zone = var.public_az
  map_public_ip_on_launch = true
  tags = {
    Name = "public_subnet1"
  }
}

resource "aws_subnet" "public_subnet2" {
  vpc_id     = local.vpc_id
  cidr_block = var.public_subnet2_cidr
  availability_zone = "us-east-1e"
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet2"
  }
}

resource "aws_instance" "web" {
  ami           = "ami-0cff7528ff583bf9a"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.public_subnet1.id
  key_name = data.aws_key_pair.NovaMackey.key_name

  tags = {
    Name = "HelloWorld"
  }
}
# HW use locals to change the values of subnets.